+++
title = "LinBits 0: Weekly PinePhone news / media roundup (week 27)"
aliases = ["2020/07/04/linbits0-weekly-pinephone-news-week27.html", "linbits0"]
date = "2020-07-04T11:15:18Z"
layout = "post"
[taxonomies]
authors = ["peter"]
tags = ["Phosh", "GloDroid", "Android", "UBports", "Cameras", "Manjaro", "AVMultiPhone", "OpenSUSE", "Librem 5", "Web Apps", "Mobian"]
categories = ["weekly update"]
+++
_I really hope I can make this a recurring thing._

This is some the best stuff that happened around the PinePhone this week. <!-- more -->_Commentary in italics._

### Software: releases and improvements
* [Phosh 0.4.0 Release]( https://source.puri.sm/Librem5/phosh/-/tags/v0.4.0). _Already in Mobian, this release brings a really neat feature: You can now automatically scale apps that are otherwise too big using the command_ `scale-to-fit <APP-ID> on` _, e.g._ `scale-to-fit gnome-calendar on` . __Edit 5th July 2020:__ _Apparently,_ `scale-to-fit` _is not in every distro automatically with the 0.4.0 release, as it is not in postmarketOS at the time of writing this remark._
* [GloDroid 0.3.0 pre-Release](https://github.com/GloDroid/glodroid_manifest/releases). _I am curious whether this is better than the previous release._
* [UBports Camera might work soon](https://gitlab.com/ubports/community-ports/pinephone/-/issues/37). _Please don‘t expect premium smartphone photography._
* [Manjaro ARM Alpha1 of Phosh for PinePhone and PineTab!](https://forum.manjaro.org/t/manjaro-arm-alpha1-with-phosh/151630). _After putting out five Alpha releases of Manjaro featuring Plasma Mobile, Manjaro now release a Phosh based image. BTW: A couple of days earlier, [a new ArchLinux ARM image was released](https://github.com/dreemurrs-embedded/Pine64-Arch/releases), too._
* [AVMultiPhone claims 20 hours battery life on PinePhone (german, linuxnews.de)](https://linuxnews.de/2020/07/avmultiphone-20-stunden-laufzeit-auf-dem-pinephone/). _[AVMultiPhone](http://archivista.ch/cms/de/aktuell-blog/avmultiphone/) is a postmarketOS-based distribution which looks more like your average desktop than a mobile OS. I could not get myself to try it yet, but that battery life sounds tempting. In a [recent blog post](http://archivista.ch/cms/de/aktuell-blog/preisgeld-1000-euro/), the creator sets out a 500 Euro bounty for getting the "wake up on phone call while in deep sleep" to work._

### Worth reading
* [Non-static Pinephone Review &mdash; roiredideas](https://roiredideas.wordpress.com/2020/07/04/pinephone-review/).
_This seems to be the only sensible way to review it right now. There is so much software and so much progress on so many ends, that it is just impossible to reach anything close to a verdict._
_My experience with PureOS differs, but every project puts out a wonky image once in a while. Also: If you want a dark theme on Phosh-based distributions,_ `gsettings set org.gnome.desktop.interface gtk-theme “Adwaita-dark”` _is your friend_.
* [Install #openSUSE on a #Pinephone device &mdash; victorhckinthefreeworld.com](https://victorhckinthefreeworld.com/2020/07/03/install-opensuse-on-a-pinephone-device/). _Interesting interview, even if you don't really care about openSUSE._

### Worth watching
* HackersGame: [PinePhone Images on the Librem 5](https://www.youtube.com/watch?v=K_RpvXSuY2Q)
* Pine64: [PinePhone disassembly and reassembly](https://www.youtube.com/watch?v=p2Q_SQKK7EQ)
* PizzaLovingNerd: [ICE SSBs on the PinePhone](https://www.youtube.com/watch?v=S_72hnUVehs). _Webapps on the PinePhone, nice!_

### Stuff I did
* I mostly used [Mobian](https://www.mobian-project.org):
  * I listened to a Podcast in the Gnome Podcasts app,
  * I managed to install `gnome-passwordsafe` from flathub, and got signed in to Nextcloud using Gnome Accounts, ticking two boxes on my "daily driver" check list,
  * I installed `nheko` to find out that the `squeekboard` keyboard does not work with it and then logged in to `fractal` to have non-encrypted Matrix.

