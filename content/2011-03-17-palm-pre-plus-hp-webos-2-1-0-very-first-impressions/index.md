+++
title = "Palm Pre Plus + HP webOS 2.1.0: Very First Impressions"
aliases = ["2011/03/17/palm-pre-plus-hp-webos-2-1-0-very-first.html"]
date = "2011-03-17T01:31:00Z"
[taxonomies]
tags = ["App Catalog", "apps", "webOS", "html5", "Palm Pre", "Palm Pre Plus", "platforms", "Preware", "webOS 2.1.0"]
categories = ["impressions"]
authors = ["peter"]
+++

Today I received the first one of the devices I had ordered earlier, a Palm Pre Plus. While I can't compare that thing to Palm Pre (it's being repaired) I can confirm that the Pre Plus feels better than the original Pre&mdash;build quality is better: The slider is snappier and feels more solid, the keyboard is nicer, too.
<!-- more -->
<center><a href="IMG_20110317_021532.jpg" style="margin-bottom: 10px"><img border="0" height="320" src="IMG_20110317_021532.jpg" width="240" /></a></center>

webOS 2.1.0 is better, too. While booting the device isn't exactly blazing fast, it seems that start up time improved a little bit. Besides that, I have to say that I like the new launcher&mdash;and I am pleased by the number of patches that are already available on Preware. While the Browser is better at HTML5 support now, Apps are still not too many, but a stroll throughout the AppCatalog, which is quite comfortable to walk through makes feel that the applications that are there are quite promising&mdash;and I am talking about the free ones here.

_More soon._
