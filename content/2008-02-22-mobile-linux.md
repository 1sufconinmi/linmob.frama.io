+++
title = "Mobile Linux..."
aliases = ["2008/02/mobile-linux.html"]
author = "peter"
date = "2008-02-22T09:58:00Z"
layout = "post"
[taxonomies]
tags = ["platforms", "LiMo", "Open Handset Alliance", "Mobile World Congress",]
categories = ["events"]
authors = ["peter"]
+++
As I was looking for some information about Mobile Linux on Mobile World Congress (MWC), I found this roundup by <a href="http://www.zdnet.co.uk/">ZDNet.co.uk</a> [<a href="http://news.zdnet.co.uk/communications/0,1000000085,39303415,00.htm">Part1</a>] [<a href="http://news.zdnet.co.uk/communications/0,1000000085,39303418,00.htm">Part2</a>]. It contains information about LiMo-Foundation and their members and&mdash;of course&mdash;about Open Handset Alliance / Google's Android platform, not much I wasn't aware of before, but it is a nice roundup about the future of commercial Linux on mobile phones.
<!-- more -->

If you are interested in &#8220;really open&#8221; stuff, you might be nonetheless disappointed, but these videos are not for an FOSS fanatic audience, so that is OK.

Regarding Android at MWC you might read<a href="http://www.mobile-review.com/exhibition/2008-3gsm-android-en.shtml"> this nice roundup by one of the best websites about mobile phones.</a>

ATM I am a bit disappointed about my E2831s lack of a VPN client which makes me unable to use WiFi at university&mdash;and I won't have the time to work on this in holidays. A910i appears to be not to be bought by geeky people yet, so it's firmware hasn't found it's way to me, yet&mdash;which means that the relatively easy &#8220;put VPNC on EZX&#8221; option does not exist.
And as I am looking for a real keyboard, I might purchase a <a href="http://wiki.xda-developers.com/index.php?pagename=HTC_Universal">HTC Universal</a>, which is said to be a nice test device for QTopia, OpenMoko and such. Maybe that would fulfill for my demand for a smaller mobile &#8220;PC&#8221; (my laptop is a 15&#8221; HP nx6325), too&#8230;. And I always wanted to have a look at &#8220;Windows Mobile&#8221;, didn't I ;)&#160;?
